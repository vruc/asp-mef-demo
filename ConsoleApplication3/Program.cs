﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            Console.WriteLine("loggly demo start");

            int count;

            int.TryParse(args.Length == 0 ? "1000" : args[0], out count);

            SendLogs(count);

            // Create a new concurrent dictionary.
            //ConcurrentDictionary<string, CityInfo> cities = new ConcurrentDictionary<string, CityInfo>();
            //if (cities.TryAdd("AAA", new CityInfo() {Name = "AAA"} ))
            //{
            //    Console.WriteLine("1. success");
            //}

            //if (cities.TryAdd("AAA", new CityInfo() {Name = "BBB"}))
            //{
            //    Console.WriteLine("2. success");
            //}
            //else
            //{
            //    Console.WriteLine("2. fail");
            //}

            Console.WriteLine("");
            Console.WriteLine("loggly demo done");
            Console.ReadKey();
        }

        private static void SendLogs(int count)
        {
            for (var i = 0; i < count; i++)
            {
                var token = Guid.NewGuid().ToString();
                Log.InfoFormat(
                    "Processing api request with Url [/api/v3/GoogleMarkerHelper?fill={2}] took {0}ms, returning status code: 200(OK), Token: [{1}], IP: [{3}].",
                    DateTime.Now.Millisecond % 100, token, token.Substring(0, 6), GenIP());

                Thread.Sleep(10);
                Console.Write(".");
            }
        }

        private static string GenIP()
        {
            var rand = new Random(DateTime.Now.Millisecond);
            return string.Format("{0}.{1}.{2}.{3}", 
                rand.Next(255), 
                rand.Next(255), 
                rand.Next(255), 
                rand.Next(255));
        }
    }

    class CityInfo
    {
        public string Name { get; set; }
        public DateTime lastQueryDate { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public int[] RecentHighTemperatures { get; set; }
    }
}
