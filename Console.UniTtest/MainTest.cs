﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
using NUnit.Framework;

namespace Console.UniTtest
{
    [TestFixture]
    public class MainTest
    {
        [Test]
        [TestCase(1,2)]
        [TestCase(3, 4)]
        [TestCase(1, 3)]
        public void Test_ShouldBeTrue(int a, int b)
        {
            var ret = MathUtil.Compare(a, b);
            Assert.IsTrue(ret);
        }
    }
}
