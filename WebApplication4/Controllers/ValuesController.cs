﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApplication4.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }

    [EnableCors(origins: "http://localhost:27675", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class TestController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var user = User;
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };
            resp.Headers.Add("X-Custom-Header", "hello");
            return resp;
        }

        public HttpResponseMessage Post()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("POST: Test message")
            };
        }

        public HttpResponseMessage Put()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent("PUT: Test message")
            };
        }
    }

    public class NormalModeBuzzerController : ApiController
    {
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }

    public class NormalModeBuzzer2Controller : ApiController
    {
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.NoContent, 1111);
        }
    }
}
