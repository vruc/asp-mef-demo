﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            var key = "myname";
            var key2 = "mynamecard";

            //init
            var memConfig = new MemcachedClientConfiguration();

            var newaddress = IPAddress.Parse("192.168.1.118");
            var ipEndPoint = new IPEndPoint(newaddress, 11211);

            // config ip
            memConfig.Servers.Add(ipEndPoint);

            // config protocal
            memConfig.Protocol = MemcachedProtocol.Binary;

            // config authentication
            memConfig.Authentication.Type = typeof(PlainTextAuthenticator);
            memConfig.Authentication.Parameters["zone"] = "";
            memConfig.Authentication.Parameters["userName"] = "memcached_user";
            memConfig.Authentication.Parameters["password"] = "A44e7omNEzInHd2gjBTl";

            // config max connection size
            memConfig.SocketPool.MinPoolSize = 5;
            memConfig.SocketPool.MaxPoolSize = 200;

            var client = new MemcachedClient(memConfig);


            var result = client.Store(Enyim.Caching.Memcached.StoreMode.Add, key, "oscar.chen");
            Console.WriteLine(result ? "Add myname success" : "Add myname fail");

            result = client.Store(Enyim.Caching.Memcached.StoreMode.Set, key, "oscar.chen --> " + Guid.NewGuid());
            Console.WriteLine(result ? "Set myname success" : "Set myname fail");

            var ret = client.Get(key);
            Console.WriteLine("Get {0}:{1}", key, ret);

            result = client.Store(Enyim.Caching.Memcached.StoreMode.Add, key2,
                new NameCard {Name = "oscar", Email = "o@o.o"});

            Console.WriteLine(result ? "Add mynamecard success" : "Add mynamecard fail");


            Console.WriteLine("press any key to continue...");
            Console.ReadKey();

        }
    }

    [Serializable]
    class NameCard
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
