﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {

            //TestConcave();
            //TestConcave2();

            var url = "! * ' ( ) ; : @ & = + $ , / ? % # [ ]".Split(' ').ToList();

            url.ForEach(a =>
            {
                Console.WriteLine("{0} {1}", a, System.Uri.EscapeDataString(a));    
            });
            
            

            Console.ReadKey();
            return;
            var polygon = new LatLng[]
            {
                new LatLng(23.000928013413937, 112.99765035510063),
                new LatLng(23.000928013413937, 112.99765035510063),
                new LatLng(23.00099714454058, 112.99994632601738),
                new LatLng(23.00099714454058, 112.99994632601738),
                new LatLng(22.998429701840447, 112.99963518977165),
                new LatLng(22.998429701840447, 112.99963518977165),
                new LatLng(22.9982914369219, 112.99765035510063),
                new LatLng(22.9982914369219, 112.99765035510063),
                new LatLng(22.999595071955312, 112.99842283129692),
                new LatLng(22.999595071955312, 112.99842283129692),
            };

            var points = new LatLng[]
            {
                new LatLng(22.99941730428301, 112.99790784716606),
                new LatLng(22.99941730428301, 112.99790784716606),
                new LatLng(22.99941730428301, 112.99790784716606),
                new LatLng(22.99941730428301, 112.99790784716606),
                new LatLng(22.9993185443639, 112.99790784716606),
                new LatLng(22.9993185443639, 112.99790784716606),
                new LatLng(22.99941730428301, 112.99790784716606),
                new LatLng(22.99941730428301, 112.99790784716606),
            };

            var i = 1;
            foreach (var point in points)
            {
                var ret = IsPointInPolygon(point, polygon);
                Console.WriteLine("{0:00#} {1}", i++ ,ret);
            }

            Console.ReadKey();


        }

        //private static void TestConcave()
        //{

        //    LatLng[] polygon = initTestData1();

        //    var points = new List<LatLng>
        //    {
        //        new LatLng(22.997985278383993, 112.9988144338131),
        //        new LatLng(22.997985278383993, 112.9988144338131),
        //        new LatLng(22.997985278383993, 112.99884662032127),
        //        new LatLng(22.997985278383993, 112.99887344241142),
        //        new LatLng(22.99797046424358, 112.99890026450157),
        //        new LatLng(22.99796058814907, 112.99892172217369),
        //        new LatLng(22.997945774005945, 112.99895390868187),
        //        new LatLng(22.997935897909628, 112.99898073077202),
        //        new LatLng(22.997935897909628, 112.99901828169823),
        //        new LatLng(22.99791120766566, 112.9990504682064),
        //        new LatLng(22.99791120766566, 112.99907192587852),
        //        new LatLng(22.99791120766566, 112.99909338355064),
        //        new LatLng(22.99791120766566, 112.9991041123867),
        //        new LatLng(22.99791120766566, 112.99911484122276)
        //    };


        //    foreach (var point in points)
        //    {
        //        var ret = IsPointInPolygon(point, polygon);
        //        Console.WriteLine(ret);
        //    }


        //    Console.ReadKey();

        //}

        //private static void TestConcave2()
        //{

        //    LatLng[] polygon = initTestData1();

        //    var points = new List<LatLng>
        //    {
        //        new LatLng(23.000819378714805, 112.99960300326347),
        //        new LatLng(23.000819378714805, 112.99960300326347),
        //        new LatLng(23.000774937221784, 112.99960300326347),
        //        new LatLng(23.000686054191828, 112.99961909651756),
        //        new LatLng(23.000597171103347, 112.99961909651756),
        //        new LatLng(23.000513225910485, 112.99961909651756),
        //        new LatLng(23.000449032492483, 112.99962982535362),
        //        new LatLng(23.00038483904395, 112.99962982535362),
        //        new LatLng(23.000330521486724, 112.99964591860771),
        //        new LatLng(23.00026632798183, 112.99964591860771),
        //        new LatLng(23.00022219492951, 112.99964591860771),
        //        new LatLng(23.000167877306843, 112.99965128302574),
        //        new LatLng(23.00013331153557, 112.99965664744377),
        //        new LatLng(23.000108621693535, 112.99965664744377),
        //        new LatLng(23.00009874575546, 112.99965664744377),
        //        new LatLng(23.0000740559071, 112.99965664744377),
        //        new LatLng(23.000054304025156, 112.99965664744377),
        //        new LatLng(23.000044428083108, 112.99965664744377),
        //        new LatLng(23.000029614168678, 112.99965664744377),
        //        new LatLng(23.000019738224815, 112.99965664744377),
        //        new LatLng(23.00000492430767, 112.99965664744377),
        //        new LatLng(23.00000492430767, 112.99969419836998),
        //        new LatLng(23.00000492430767, 112.99974784255028),
        //        new LatLng(22.99999998633493, 112.99979612231255),
        //        new LatLng(22.999995048362, 112.99983367323875),
        //        new LatLng(22.999985172415617, 112.99986585974693),
        //        new LatLng(22.999985172415617, 112.9999141395092),
        //        new LatLng(22.999985172415617, 112.99994096159935),
        //        new LatLng(22.999985172415617, 112.99994632601738),
        //        new LatLng(22.999970358494686, 112.99994632601738)
        //    };


        //    foreach (var point in points)
        //    {
        //        var ret = IsPointInPolygon(point, polygon);
        //        Console.WriteLine(ret);
        //    }

        //    Console.ReadKey();

        //}

        //private static LatLng[] initTestData1()
        //{
        //    List<LatLng> points = new List<LatLng>();

        //    points.Add(new LatLng(23.0009823307307, 113.00021957606077));
        //    points.Add(new LatLng(23.0009823307307, 113.00021957606077));
        //    points.Add(new LatLng(23.00097245485655, 113.00009083002806));
        //    points.Add(new LatLng(23.00094282722977, 112.99994632601738));
        //    points.Add(new LatLng(23.000913199596475, 112.99978539347649));
        //    points.Add(new LatLng(23.000873696075292, 112.9996620118618));
        //    points.Add(new LatLng(23.000844068426833, 112.9995547235012));
        //    points.Add(new LatLng(23.000819378714805, 112.99947425723076));
        //    points.Add(new LatLng(23.000789751054413, 112.99942597746849));
        //    points.Add(new LatLng(23.00074037160598, 112.99934551119804));
        //    points.Add(new LatLng(23.00068111624401, 112.99925431609154));
        //    points.Add(new LatLng(23.000607047004962, 112.99919530749321));
        //    points.Add(new LatLng(23.000537915678517, 112.99913093447685));
        //    points.Add(new LatLng(23.000483598182893, 112.99907192587852));
        //    points.Add(new LatLng(23.000330521486724, 112.99893781542778));
        //    points.Add(new LatLng(23.000232070858555, 112.99888953566551));
        //    points.Add(new LatLng(23.00002467619684, 112.99879297614098));
        //    points.Add(new LatLng(22.999911102794673, 112.99874469637871));
        //    points.Add(new LatLng(22.999792591316613, 112.99869641661644));
        //    points.Add(new LatLng(22.999688893687974, 112.99865886569023));
        //    points.Add(new LatLng(22.99957038201482, 112.99862667918205));
        //    points.Add(new LatLng(22.99949137417491, 112.998615950346));
        //    points.Add(new LatLng(22.99939261431001, 112.9985998570919));
        //    points.Add(new LatLng(22.99932842035906, 112.9985998570919));
        //    points.Add(new LatLng(22.999244474377157, 112.99862131476402));
        //    points.Add(new LatLng(22.999130900318534, 112.99869641661644));
        //    points.Add(new LatLng(22.99898769810843, 112.99882516264915));
        //    points.Add(new LatLng(22.998938318000743, 112.99886271357536));
        //    points.Add(new LatLng(22.998869185819647, 112.99892172217369));
        //    points.Add(new LatLng(22.998745735408203, 112.99908801913261));
        //    points.Add(new LatLng(22.998681541149644, 112.99918994307518));
        //    points.Add(new LatLng(22.99859759476545, 112.99929723143578));
        //    points.Add(new LatLng(22.998533400436436, 112.9994098842144));
        //    points.Add(new LatLng(22.998484020162557, 112.99951180815697));
        //    points.Add(new LatLng(22.99841982577954, 112.99963518977165));
        //    points.Add(new LatLng(22.99839019759247, 112.99972638487816));
        //    points.Add(new LatLng(22.998271684779112, 113.00010155886412));
        //    points.Add(new LatLng(22.998271684779112, 113.00019275397062));
        //    points.Add(new LatLng(22.998271684779112, 113.0003322288394));
        //    points.Add(new LatLng(22.998281560850874, 113.00039123743773));
        //    points.Add(new LatLng(22.9983062510271, 113.00043415278196));
        //    points.Add(new LatLng(22.998335879232602, 113.00051461905241));
        //    points.Add(new LatLng(22.99835563136597, 113.00054680556059));
        //    points.Add(new LatLng(22.99839513562409, 113.00062727183104));
        //    points.Add(new LatLng(22.998444515930473, 113.00065945833921));
        //    points.Add(new LatLng(22.99848895819077, 113.00069700926542));
        //    points.Add(new LatLng(22.998548214515058, 113.00071846693754));
        //    points.Add(new LatLng(22.998725983331905, 113.00077211111784));
        //    points.Add(new LatLng(22.998775363517247, 113.00077211111784));
        //    points.Add(new LatLng(22.998800053603155, 113.00077211111784));
        //    points.Add(new LatLng(22.998839557731202, 113.00077211111784));
        //    points.Add(new LatLng(22.99887906184768, 113.00077211111784));
        //    points.Add(new LatLng(22.99889881390159, 113.00076138228178));
        //    points.Add(new LatLng(22.998938318000743, 113.00074528902769));
        //    points.Add(new LatLng(22.99897782208833, 113.00071846693754));
        //    points.Add(new LatLng(22.999012388155485, 113.0006916448474));
        //    points.Add(new LatLng(22.999037078198036, 113.00065409392118));
        //    points.Add(new LatLng(22.99907658225672, 113.00061654299498));
        //    points.Add(new LatLng(22.9991506523356, 113.00050389021635));
        //    points.Add(new LatLng(22.99919015636104, 113.00038050860167));
        //    points.Add(new LatLng(22.99922966037493, 113.00032686442137));
        //    points.Add(new LatLng(22.99924941237753, 113.00026249140501));
        //    points.Add(new LatLng(22.999264226377594, 113.00021421164274));
        //    points.Add(new LatLng(22.999308668368027, 113.000080101192));
        //    points.Add(new LatLng(22.999323482361575, 113.00005327910185));
        //    points.Add(new LatLng(22.99932842035906, 113.00001606345177));
        //    points.Add(new LatLng(22.999338296353496, 112.99997851252556));
        //    points.Add(new LatLng(22.999353110343815, 112.99995705485344));
        //    points.Add(new LatLng(22.999367924332493, 112.99991950392723));
        //    points.Add(new LatLng(22.999362986336447, 112.99990341067314));
        //    points.Add(new LatLng(22.999387676314864, 112.99984440207481));
        //    points.Add(new LatLng(22.999397552304977, 112.99979612231255));
        //    points.Add(new LatLng(22.99941236628877, 112.99975320696831));
        //    points.Add(new LatLng(22.99941236628877, 112.99971029162407));
        //    points.Add(new LatLng(22.99942224227706, 112.99968883395195));
        //    points.Add(new LatLng(22.999437056258156, 112.99964055418968));
        //    points.Add(new LatLng(22.999446932244652, 112.99959227442741));
        //    points.Add(new LatLng(22.999461746223023, 112.9995547235012));
        //    points.Add(new LatLng(22.999471622207714, 112.99951180815697));
        //    points.Add(new LatLng(22.99948643618338, 112.99949571490288));
        //    points.Add(new LatLng(22.999506188148423, 112.99942597746849));
        //    points.Add(new LatLng(22.999521002120307, 112.99939915537834));
        //    points.Add(new LatLng(22.999521002120307, 112.99938306212425));
        //    points.Add(new LatLng(22.99953581609055, 112.99938306212425));
        //    points.Add(new LatLng(22.99953087810066, 112.99936696887016));
        //    points.Add(new LatLng(22.99954569206982, 112.9993562400341));
        //    points.Add(new LatLng(22.999550630059176, 112.99934014678001));
        //    points.Add(new LatLng(22.999575320003284, 112.9993025958538));
        //    points.Add(new LatLng(22.99957038201482, 112.9993025958538));

        //    return points.ToArray();
        //}

        private static bool IsPointInPolygon(LatLng p, LatLng[] polygon)
        {
            double minX = polygon[0].Latitude;
            double maxX = polygon[0].Latitude;
            double minY = polygon[0].Longitude;
            double maxY = polygon[0].Longitude;

            for (int i = 1; i < polygon.Length; i++)
            {
                LatLng q = polygon[i];
                minX = Math.Min(q.Latitude, minX);
                maxX = Math.Max(q.Latitude, maxX);
                minY = Math.Min(q.Longitude, minY);
                maxY = Math.Max(q.Longitude, maxY);
            }

            if (p.Latitude < minX || p.Latitude > maxX || p.Longitude < minY || p.Longitude > maxY)
            {
                return false;
            }

            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            bool inside = false;
            for (int i = 0, j = polygon.Length - 1; i < polygon.Length; j = i++)
            {
                if ((polygon[i].Longitude > p.Longitude) != (polygon[j].Longitude > p.Longitude) &&
                     p.Latitude < (polygon[j].Latitude - polygon[i].Latitude) * (p.Longitude - polygon[i].Longitude) / (polygon[j].Longitude - polygon[i].Longitude) + polygon[i].Latitude)
                {
                    inside = !inside;
                }
            }

            return inside;
        }
    }



    class LatLng
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public LatLng(double latidude, double longitude)
        {
            Latitude = latidude;
            Longitude = longitude;
        }
    }
}
